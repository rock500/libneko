``libneko.attr_generator``
==========================


**A small system for associating attributes with commands.**


.. inheritance-diagram:: libneko.attr_generator

.. automodule:: libneko.attr_generator
    :members:
    :inherited-members:
    :show-inheritance:
