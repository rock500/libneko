``libneko.asyncinit``
=====================

**Base class implementation for creating classes that have an asynchronous constructor that has to be awaited.**

.. inheritance-diagram:: libneko.asyncinit

.. automodule:: libneko.asyncinit
    :members:
    :inherited-members:
    :show-inheritance:
