``libneko.singleton``
=====================

**Working singleton object metaclass implementation.**

.. inheritance-diagram:: libneko.singleton

.. automodule:: libneko.singleton
    :members:
    :inherited-members:
    :show-inheritance:
