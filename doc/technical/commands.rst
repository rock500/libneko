``libneko.commands``
====================

**Extensions to the** ``discord.ext.commands`` **submodule. This includes utilities for adding acknowledgement emojis**
**to an invoking command; implementations of** ``Command`` **and** ``Group`` **that do more things; and a simple**
**labelling attribute system for applying certain flags to various commands. The latter can be useful for adding**
**information such as the default permission to allow for executing the command, if you were to implement customisable**
**permissions in your bot. There is also a system to produce a usage message for command groups with no other body in**
**the main callback.**

.. inheritance-diagram:: libneko.commands discord.ext.commands

.. automodule:: libneko.commands
    :members:
    :inherited-members:
    :show-inheritance:


