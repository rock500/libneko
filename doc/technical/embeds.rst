``libneko.embeds``
==================

**An embed class that will do what you want, rather than be a nuisance to use.**

.. inheritance-diagram:: libneko.embeds

.. automodule:: libneko.embeds
    :members:
    :inherited-members:
    :show-inheritance:


Note:
    This class will eventually implement client-side validatation. No more
    slow, obfuscated ``HTTPException`` errors to tell you that you have an empty
    string somewhere.
