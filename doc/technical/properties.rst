``libneko.properties``
======================

**All kinds of special property implementations, and cached properties for** ``__dict__`` **and**
``__slot__`` **classes!**

.. inheritance-diagram:: libneko.properties

.. automodule:: libneko.properties
    :members:
    :inherited-members:
    :show-inheritance:

