``libneko.pag``
===============

**Customisable, modular paginator utilities.**

The ``pag`` submodule consists of four major components:

- The ``paginator``:
    this takes paragraphs, pages, and other bits of strings
    and outputs these bits in sized bins. In essence: bits of text go in: formatted
    pages that are within the specified character limit come out. This is the
    bread and butter behind how this submodule ensures your content will fit into
    a single Discord message. You can also pass multiple optional ``Substitutions``
    to this paginator to sanitise internal markdown formatting.

- The ``navigator``:
    a state machine that adds ``reactionbuttons`` to a message on
    Discord. These ``reactionbuttons`` are programmable and can allow the changing
    of the displayed page from a collection of pages produced by the ``paginator``
    or from custom-defined pages. Each button is added as a reaction by the navigator
    itself. You can then define who is allowed to trigger any of these reactions.

- The ``reactionbuttons``:
    an abstraction of what is essentially a Discord message
    reaction. These are defined with some custom logic (up to you what that logic
    actually is), and have an emoji. They get added to a ``navigator`` and that
    handles displaying them. When a user adds a reaction corresponding to an existing
    ``reactionbutton``, the ``navigator`` can remove that reaction and perform the
    logic you defined. That can be anything from changing a page, to kicking a user.
    You can do hopefully pretty much anything with this.

    There are also a bunch of predefined buttons in this module that you can use
    instead.

- The ``factory``:
    everyone hates making loads of objects repeatedly to do the same
    boring shit over and over again. Factories come to the rescue. Now, you can
    make a single object that behaves as a paginator, then run ``build`` or ``start``
    and boom: working ``navigator`` that is ready to use.

    Embed navigation factories are also now supported (experimentally). These are
    slightly more complicated, since Embed objects are much more customisable than
    markdown-formatted messages.

.. note::
    One should remain aware that a ``factory`` is, essentially a special implementation
    of ``paginator``. That means it will do everything that a ``paginator`` will do, to
    the same effect. The main difference is that to get meaningful output, you can call
    the built in ``start`` or ``build`` methods to get the whole thing working.

.. warning::
    Discord desktop clients have a known bug that occurs when editing a message or
    embed that has bold/underlined markdown in it. This can lead to the text appearing
    scrambled. Don't worry if this happens: it is not your fault! See: https://trello.com/c/Nnkj5D0W
    for the bug report. If you change to a different channel, and then back to the
    current channel, it will appear to be fixed.

    To work around this, you need to remove markdown formatting. It then appears to work
    properly.


**Inheritance for paginator and factories:**


.. inheritance-diagram:: libneko.pag.paginator libneko.pag.factory.embedfactory libneko.pag.factory.stringfactory


**Inheritance for navigators:**

.. inheritance-diagram:: libneko.pag.abc libneko.pag.navigator


``paginator``
-------------

**Taking bits of text and making big chunks that will not exceed a given number of characters and/or lines. Like**
``discord.ext.commands.Paginator`` **on 'roids.**

.. automodule:: libneko.pag.paginator
    :members:
    :inherited-members:
    :show-inheritance:

``navigator``
-------------

**Allowing the user to navigate through multiple pages of messages using reactions on a single Discord message.**

.. automodule:: libneko.pag.navigator
    :members:
    :inherited-members:
    :show-inheritance:


``reactionbuttons``
-------------------

**Reactions to a Discord message.**

.. automodule:: libneko.pag.reactionbuttons
    :members:
    :inherited-members:
    :show-inheritance:


``factory``
-----------

**Making navigators quickly and simply.**

.. automodule:: libneko.pag.factory.stringfactory
    :members:
    :inherited-members:
    :show-inheritance:


.. automodule:: libneko.pag.factory.embedfactory
    :members:
    :inherited-members:
    :show-inheritance:


``abc``
-------

**No one really cares about this. It is not that useful unless you are debugging
memory usage, but I come from Java. Abstract classes are like a fetish for us.**

.. automodule:: libneko.pag.abc
    :members:
    :inherited-members:
    :show-inheritance:

``utilities``
-------------

**Extra bits and pieces for lazy people.**

.. autoclass:: libneko.pag.optionpicker.NoOptionPicked
    :members:
    :inherited-members:
    :show-inheritance:

.. autofunction:: libneko.pag.optionpicker.option_picker
    :members:
    :inherited-members:
    :show-inheritance:

