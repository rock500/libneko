``libneko.funcmods``
====================

**Extensions to the** ``functools`` **module. This includes a better** ``wraps`` **method, and a bunch of utilities**
**to fool the typechecker.**


.. inheritance-diagram:: libneko.funcmods

.. automodule:: libneko.funcmods
    :members:
    :inherited-members:
    :show-inheritance:

