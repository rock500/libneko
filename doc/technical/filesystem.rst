``libneko.filesystem``
======================

**File-access utilities and helpers.**

**Module listing:**

.. automodule:: libneko.filesystem
    :members:
    :inherited-members:
    :show-inheritance:
